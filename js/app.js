/*
 * Space Age Calculator
 * Created for a hackathon competition in 2017.
 */
function Race(){
	this.pending=0;
	this.callbacks=[];
}
Race.prototype={
	start:function(){
		console.log("Started");
		this.pending++;
	},
	onfinish:function(callback){
		this.callbacks.push(callback);
	},
	finish:function(){
		if (this.pending<=0){
			console.warn('More finishes than starters in race!');
		}
		console.log("Finished");
		if (--this.pending==0){
			// Finished, fire callbacks
			for (var g=0,glen=this.callbacks.length;g<glen;g++){
				this.callbacks[g]();
			}
		}
	},
	reset:function(){
		this.pending=0;
		this.callbacks=[];
	}
};

var app=(function(){
	// An earth year is 31557600 seconds.
	// We don't want to be Earth-centered; our solar system is all inclusive.
	// These values are the number of seconds that each body takes to orbit the sun.
	var conversion={
		mercury:7600543.81992,
		venus:19414149.052176,
		tellus:31558144.32,
		mars:59354032.69008,
		jupiter:374355659.124,
		saturn:929292362.8848,
		uranus:2651370019.3296,
		neptune:5200418560.032,
		pluto:7826284800
	};

	var bodies={
		mercury:"Mercury",
		venus:"Venus",
		tellus:"Earth",
		mars:"Mars",
		jupiter:"Jupiter",
		saturn:"Saturn",
		uranus:"Uranus",
		neptune:"Neptune",
		pluto:"Pluto"
	};

	var images={
		mercury:'img/mercury.jpg',
		venus:'img/venus.jpg',
		tellus:'img/tellus.jpg',
		mars:'img/mars.jpg',
		jupiter:'img/jupiter.jpg',
		saturn:'img/saturn.jpg',
		uranus:'img/uranus.jpg',
		neptune:'img/neptune.jpg',
		pluto:'img/pluto.jpg'
	};

	function Side(el,body,value,text){
		this.id=sides.length;
		this.value=25;
		this.el=el;
		this.text=text;

		this.setBody(body);
	}
	Side.prototype={
		maxlength:10,
		generateSelect:function(){
			this.options=$('>ul');

			this.select=$('>div')
				.class('dropdown')
				.on('click',function(evt){
					evt.stopPropagation();
					$('div.dropdown').removeClass('active');
					$('.tooltip.body').css({opacity:0}); // Hide tooltips for body
					this.addClass('active');
				})
				.append(
					$('>span')
					.class('editable')
					.text(bodies[this.body]),
					this.options
				)

			this.updateSelectOptions();
		},
		generateContent:function(){
			this.yearstext=$('>span').text(' years');

			this.generateSelect();
			this.generateInput();

			this.content=$('>div')
				.class('input')
				.append(
					$('>div').class('test'),
					$('>p').append(
						$('>span')
						.text(this.text[0]),
						this.select
					),
					$('>p').append(
						$('>span')
						.text(this.text[1]),
						this.input,
						this.pseudoinput,
						this.yearstext
					)
				);
		},
		generateImg:function(){
			if (!this.body) throw new Error('Cannot generate img for empty body!');

			var self=this;
			this.img=$('>img')
				.class('background')
				.set({src:images[this.body]})
				.on('load',function(){
					console.log(this.prop('complete'));
					this.css({opacity:1,transform:"translateX(0px)"});
					self.content.css({opacity:1,transform:"translateX(0px)"});
				});
		},
		generateInput:function(){
			var self=this;

			var input,pseudoinput;

			this.input=input=$('>input')
				.set({type:'tel',maxlength:this.maxlength})
				.class('hidden')
				.value(this.value)
				.on('input',function(){
					var value=this.value();

					// Update the input text
					self.updateValueFromInput(value);

					// Update our own years text
					self.updateYearsText();

					// Do the actual conversion
					app.changed(self.id);
				})
				.on('blur',function(){
					this.class('hidden');
					pseudoinput.removeClass('hidden').class('editable');
				})
				.on('change',function(){
					// Ensure we have a number here
					var t=this.value()||"0";
					t=parseFloat(t.replace(/[^0-9.]/g,''));
					this.value(t);
					pseudoinput.text(t);
					this.blur();
				});

			this.pseudoinput=pseudoinput=$('>button')
				.text(this.input.value())
				.class('editable')
				.on('click',function(){
					// Swap between a span and the actual input (hence the name 'pseudoinput')
					input.removeClass('hidden').focus();
					this.class('hidden');
					$('.tooltip.age').css({opacity:0}); // Hide tooltips for age
				});
		},
		generate:function(){
			this.generateImg();
			this.generateContent();
		},
		setBody:function(body){
			this.body=body;

			this.generate();
			this.updateElements();

			this.el
				.clear()
				.append(
					this.img,
					this.content
				)
		},
		changeTo:function(newBody){
			var self=this;
			this.img.one('transitionend',function(){
				self.setBody(newBody);
				app.changed(0);
			}).css({opacity:0,transform:"translateX(50px)"});
			this.content.css({opacity:0,transform:"translateX(75px)"});
			this.el.get('.tooltip').css({opacity:0}); // Hide tooltips as well
		},
		updateSelectOptions:function(){
			var options=[],self=this;

			// We don't want to display these. (ones that are already displayed, including ours)
			var badbodies=[];
			for (var s in sides) badbodies.push(sides[s].body);

			console.log(badbodies);

			for (var b in bodies){
				(function(b){
					if (badbodies.indexOf(b)+1) return;

					var opt=$('>li')
						.text(bodies[b])
						.on('click',function(){
							self.changeTo(b);
						});

					options.push(opt);
				})(b);
			}

			this.options.clear().append(options);
		},
		updateValueFromInput:function(value){
			value=value||"0";

			var trail='';
			value.replace(/^[1-9][0-9]*(\.0*)$/,function(_,_1){
				// Special case: do not clear a trailing period or zeroes when we're typing stuff.
				// i.e. "2." is technically incorrect, but if we corrected that to "2", we could never type decimals.
				// The same way, 10.0 would get corrected to 10 before we could even type 10.05
				trail=_1;
			});

			this.value=parseFloat(parseFloat(value.replace(/[^0-9.]/g,'')).toFixed(4));

			this.input.value(this.value+trail);
			this.pseudoinput.text(this.value+trail);
		},
		updateYearsText:function(){
			text=' ';
			text+=this.value==1?'year':'years';
			text+=this.text[2];
			this.yearstext.text(text);
		},
		updateInputText:function(){
			var value=parseFloat(this.value.toFixed(4));
			this.input.value(value);
			this.pseudoinput.text(value);
		},
		updateValue:function(value){
			this.value=value;
			this.updateInputText();
			this.updateElements();
		},
		updateElements:function(){
			this.updateYearsText();
			this.updateInputText();
			this.updateSelectOptions();
		},
		show:function(){
			this.el.css({opacity:1});
		}
	};

	var focus=0;
	var sides=[];

	var from=$('>div').class('side from'),to=$('>div').class('side to');

	sides.push(new Side(from,'tellus',25,['If your age on',' was ','...']));
	sides.push(new Side(to,'saturn',0,['...then on',' you\'d be ',' old!']));

	$.load(function(){
		$('#loading').text('Loading...');

		app.changed(0); // Update values for the first time

		$(document.body).on('click',function(){
			// Make all dropdowns inactive when we click somewhere else
			$('div.dropdown').removeClass('active');
		});

		var imageload=new Race();

		imageload.onfinish(function(){
			$("#loading").css({opacity:0});

			$(document.body).append(
				from,
				to,
				$('>div')
				.class('modal top')
				.append(
					$('>div')
					.class('swap')
					.append(
						$('>button')
						.text('Swap?')
						.on('click',function(){
							app.swapSides();
						})
					)
				),
				$('>div')
				.class('modal bottom')
				.append(
					$('>div')
					.class('legal')
					.append(
						$('>p').text('All the pictures on this page are courtesy of NASA/JPL-Caltech'),
						$('>p').text("Check them out! ").append(
							$('>a').set({href:"https://www.nasa.gov/multimedia/imagegallery/"}).text("https://www.nasa.gov/multimedia/imagegallery/")
						)
					)
				)
			);

			// Insert tooltips, position using JavaScript because why not, we're already using it.
			// Let's be honest here. I forgot about the tooltips until the last second, and this is a bad implementation.

			$('.input')[0]
				.prepend(
					$('>p')
					.class('tooltip top body')
					.text('Try changing the planetary body!')
					.css({
						transform:"translateX(-50px)",
						opacity:0
					})
					.on('click',function(){
						this.css({opacity:0});
					})
				)
				.parent()
				.append(
					$('>p')
					.class('tooltip bottom age')
					.text('Or your age!')
					.css({
						transform:"translateX(-83px)",
						opacity:0
					})
					.on('click',function(){
						this.css({opacity:0});
					})
				);

			// Time out the stages so people can stay amazed longer
			setTimeout(function(){
				sides[0].show();
			},500);
			setTimeout(function(){
				sides[1].show();
			},2000);
			setTimeout(function(){
				$(".tooltip.body").css({opacity:1});
			},5000);
			setTimeout(function(){
				$(".tooltip.age").css({opacity:1});
			},7000);
		});

		// Preload images, wait for a second in any case
		imageload.start();
		setTimeout(function(){
			imageload.finish();
		},1000);

		//var imgs=[];
		//for (var g in images){
		//	imageload.start();
		//	imgs[g]=new Image();
		//	imgs[g].onload=function(){
		//		imageload.finish();
		//	}
		//	imgs[g].src=images[g];
		//}
	});

	return {
		changed:function(changedSideId){
			// Update all other sides according to this side
			var changedSide=sides[changedSideId];

			changedSide.updateSelectOptions();

			for (var s=0,ss=sides.length;s<ss;s++){
				if (s==changedSideId) continue;
				var side=sides[s];

				// Calculate conversion
				side.updateValue(
					this.convert.to(
						side.body,
						this.convert.from(
							changedSide.body,
							changedSide.value
						)
					)
				);
			}
		},
		swapSides:function(){
			var configs=[];
			for (var s=0,ss=sides.length;s<ss;s++){
				configs.push({value:sides[s].value,body:sides[s].body});
			}
			for (var c=0,cc=configs.length;c<cc;c++){
				sides[cc-c-1].value=configs[c].value;
				sides[cc-c-1].changeTo(configs[c].body);
			}
		},
		convert:{
			to:function(body,saage){
				if (!(body in conversion)) {
					throw new Error("No conversion found for body '"+body+"'.");
				}
				return saage/conversion[body];
			},
			from:function(body,saage){
				if (!(body in conversion)) {
					throw new Error("No conversion found for body '"+body+"'.");
				}
				return conversion[body]*saage;
			}
		}
	};
})();
